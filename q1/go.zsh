#! /usr/bin/env zsh
alias @='for it'
setopt braceccl extendedglob nounset warncreateglobal

get_recent_files () {
    seq 201{5,7} |
        xargs -P4 -IYYYY curl -slkoYYYY https://vote.gnome.org/YYYY/electorate.txt
    # remove the title lines
    sed -i 1,2d 201[56]
}

get_recent_files

get_old_data () {
    @ ( {2000..2005} ) {
        print "$( curl -slk https://vote.gnome.org/$it/voters.html |
        sed -rn 's/[^0-9]*([0-9]+) +voters.*/\1/p' ) $it" }
}

get_old_data > old.data

{ echo Voters Years
    cat old.data
    @ ( {2006..2014} ) echo -- \? $it

    # the recent ones (get_recent_files first)
    wc -l 201[5-7] |
    sed -r ' $d ; s/^ +//'

} | awk '{print $2, $1}' > results.data

{ echo year candidates
    cut -f1 -d\; ../q2/full.csv |
    sort                        |
    uniq -c                     |
    sed '/2007/s/$/\n? 2008/'   |
    awk '{print $2,$1}' } > candidates.data

gnuplot <<\.
set terminal svg
set datafile missing "?"
set title "voters by years"
set output 'show.svg'
set key autotitle columnheader
set yrange[0:]
# TODO: dashed between 2005 and 2015
plot 'results.data' u 1:($2) with lp
.

gnuplot <<-\SCRIPT
set terminal svg
set output 'dump.svg'
set grid
set title "voters and abst. by years"
set key autotitle columnheader
set yrange[0:]
set y2tics
set y2label "candidates"
set ylabel  "voters and abst"
# set y2range[0:30]
plot for [i=2:3] 'dump.data' u 1:i w lp, 'candidates.data' u 1:2 w lp axis x1y2
SCRIPT




