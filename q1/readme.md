
the script [go.zsh](go.zsh) produced the numbers of voters by years
stored in [results.data](results.data) and the plotted version

![](show.svg)

i also tried to render old html pages as csv files. there are some
errors in it and need to be investigated.

[get_old_data_as_csv.zsh](get_old_data_as_csv.zsh) produced

* [2000](2000)
* [2001](2001)
* [2002](2002)
* [2003](2003)
* [2004](2004)
* [2005](2005)

we got [dump.svg](dump.svg) according to [data](dump.data) extracted from a gnome DB.
