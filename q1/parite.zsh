perl -F\; -lnE '
    ($year, $gender ) = @F[0,3];
    $with{ $year }{ $gender }++;
    END {
        for my $year (sort keys %with) {
            s/$/.0/ for values %$_;
            say "$year ${\( $$_{F} / ( $$_{M} + $$_{F}) )}"
                for $with{$year}
        }
    }
' candidates_years_affiliation_gender.csv > parite.data


gnuplot <<-\SCRIPT
set terminal svg
set output 'parite.svg'
set grid
set title "M / W"
set key autotitle columnheader
set yrange[0:1]
plot 'parite.data' u 1:2 w lp
SCRIPT




