# try to get old voters data as CSV
# 2 strategies there
# * curl + sed
# * lynx -dump + sed
# BOTH HAVE ERRORS.
# must be fixed (no time to investigate)

old_voters_page_to_csv () {
    sed -rn '
        /@no_spam/ {
            # remove all html tags
            s/[<][^>]+[>]//g

            # make it csv
            s/ *&[lg]t; */;/g
            s/;$//g

            # trim and print
            s/^ +//
            s/ +$//
            p
    }' "$@"
}

get_old_files_as_text () {
    @ ( {2000..2005} ) {
        curl -slk https://vote.gnome.org/$it/voters.html |
            old_voters_page_to_csv > $it }

}

get_old_files_as_text_using_lynx () {
    @ ( {2000..2005} ) {
        lynx -dump https://vote.gnome.org/$it/voters.html |
            sed -rn '/@no_spam/,/^ *$/p' |
            tac |
            sed '
                # first line is empty line from the other sed
                1d

                # no bullet means extra information for the
                # current entry
                # we need to merge all those lines
                # (that is why we used tac)

                /^ *\*/! { x ; n ; G ; s/ *\n */ / }

            ' > $it & }
}
