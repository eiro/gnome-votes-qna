#

the script [go.zsh](go.zsh) produced

* [csv of candidates for year 2010](2010.csv)
* [csv of candidates for year 2011](2011.csv)
* [csv of candidates for year 2012](2012.csv)
* [csv of candidates for year 2013](2013.csv)
* [csv of candidates for year 2014](2014.csv)
* [csv of candidates for year 2015](2015.csv)
* [csv of candidates for year 2016](2016.csv)
* [csv of candidates for year 2017](2017.csv)
* [csv of candidates for all years (year added as first column](full.csv)
* then it seems to be 4 teams in the Gnome Fondation so we also added a
  [data for gnuplot](plot.data)

so we get

![](show.svg)

as alternative rendering, we made [all.zsh](all.zsh) and get

![](show_all.svg)

