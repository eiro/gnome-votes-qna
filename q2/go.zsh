#! /usr/bin/env zsh
alias @='for it'
alias l='print -l'
setopt braceccl extendedglob nounset warncreateglobal

# TODO: 2000-2003 as there is no affiliation in it :(

download () {
    l {2000..2007} {2009..2017} |
        xargs -P4 -IYYYY curl -slkoYYYY.html https://vote.gnome.org/YYYY/candidates.html }

html2csv () {
    # reverse the flow of the file because
    # Affiliation is a usable anchor and the line before it is important

    tac | sed -rn '/Affiliation:/ {

        # read the candidate
        N

        # remove all html tags
        s/[<][^>]+[>]//g

        # join lines with a ; and remove extra (optional) numbered list
        s/ *\n *([0-9]+[.] *)?/;/

        # trim and print
        s/^ +//
        s/ +$//
        p

    }' | sed -r "s/^Affiliation: *//" | tac }


test -f 2000.html || download
test -f 2004.csv  || {
    @ (20<04-17>.html) { html2csv < $it > $it:t:r.csv }
    @ (20<04-17>.csv)  { sed "s/^/$it:t:r;/" $it  } > full.csv }



perl -wnE '
    BEGIN {

        %matches_company =
        ( "RedHat"  => qr/^Red Hat/
        , "Endless" => qr/^Endless/
        , "None"    => qr/^None/ );

        @most_seen = sort keys %matches_company;

    }

    # 3rd col is the candidate (not used)
    ( $year, $company ) = split /;/;

    # the first company with a maching pattern
    # or "Other" as it is the only one always
    # in the list

    ($belongs_to) =
        (( map {
            $company =~ $matches_company{$_}
            ? $_ : () } @most_seen )
            , "Other");

    $for_year{$year}{$belongs_to}++;

    END {
        say
        join " "
            , qw( Year Other )
            , @most_seen;
        for my $year (sort keys %for_year) {
            say join " "
            , $year
            , map
                { $for_year{$year}{$_} || 0 }
                "Other", @most_seen
        }
    }

' full.csv > plot.data

gnuplot <<-\SCRIPT
set terminal svg
set output 'show.svg'
set grid
set title 'Candidate affiliations'
set key autotitle columnheader
set yrange[0:]
plot for [i=2:5] 'plot.data' u 1:i w lp
SCRIPT


