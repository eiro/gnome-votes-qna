perl -MYAML -F\; -lnE '
    my ( $year, $company ) = @F;

    map {
        s/^ +| +$//g;
        if (length) {
            $_ = lc $_;
            $_ = "none" if /\bnone\b/;
        } else {
            $_ = "none";
        }
        s/[ ,].*//;
    } $company;
    $for_year{ $year }{ $company }++;

    END {
        map { map $company{$_}++, keys %$_ } values %for_year;
        map {
            $company{$_} > 3 or
                $company{other}+= ( $other{$_} = delete $company{$_} );
        } keys %company;
        @companies = sort keys %company;
        $for_year{2008} = { map +( $_ => q(?) ), @companies };

        say join q( ), Year => @companies;
        for my $year ( sort keys %for_year ) {
            say join q( )
            , $year
            , map { $for_year{ $year }{ $_ } || 0 } @companies;
        }
        map { say "# $_ : $other{$_}" } keys %other
    }
' full.csv > trimed_companies.csv

gnuplot <<-\SCRIPT
set terminal svg
set output 'show_all.svg'
set grid
set title 'Candidate affiliations'
set key autotitle columnheader
set datafile missing "?"
plot for [i=2:10] 'trimed_companies.csv' u 1:i w lp
SCRIPT


